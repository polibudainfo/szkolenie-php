<?php

class Witacz{

  private $myName;

  function __construct($name){
     $this->myName = $name ? $name : 'Anonymous';
  }

  public function sayHi(){
    echo "Hello ".$this->myName;
  }
}

$witaczObj = new Witacz(isset($_REQUEST['name']) ? $_REQUEST['name'] : null);
$witaczObj->sayHi();



?>

<form method="POST">
  <input type="text" name="name"/>

  <button type="submit">Say Hi</button>
</form>